import React, {useState} from "react";
// node.js library that concatenates classes (strings)

import Router from 'next/router'



import classnames from "classnames";
// javascipt plugin for creating charts

// react plugin used to create charts

// reactstrap components
import {
  Button,
  Card,
  CardHeader,
  CardBody,
  NavItem,
  NavLink,
  Nav,
  Progress,
  Table,
  Container,
  Row,
  Col,
  Form,
  FormGroup,
  Label,
  Input,
  FormText
} from "reactstrap";
// layout for this page
import Admin from "layouts/Admin.js";
// core components
import AppHelper from '../../app-helper'


import Header from "components/Headers/Header.js";

const Dashboard2 = () => {

  const [categoryName,setCategoryName] = useState(undefined)
  const [type,setTypeName] = useState(undefined)
  const [amount,setAmount] = useState(0)
  const [description,setDescription] = useState("")
  const [categories,setCategories] = useState([]);

    const getCategories = (value) => {
        setTypeName(value)

        const payload = {
            method:'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${AppHelper.getAccessToken()}`
            },
            body: JSON.stringify({type:value})
        }

        //send a request using fetch
        fetch(`https://morning-coast-98421.herokuapp.com/api/users/get-categories`, payload).then(AppHelper.toJSON).then(data => {
            setCategories(data)
        })
    }

    const createRecords = (e) => {
        e.preventDefault()

        const payload2 = {
            method:'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${AppHelper.getAccessToken()}`
            },
            body: JSON.stringify({
                categoryName: categoryName,
                type: type, 
                amount: amount,
                description: description
            })
        }
        //send the request to the backend project for processing
        fetch(`https://morning-coast-98421.herokuapp.com/api/users/add-record`, payload2).then(AppHelper.toJSON).then(newRecordData => {
            //we will describe the procedure upon receiving the response.
            console.log(newRecordData)
            if (newRecordData === true) {
                alert('Record Added', 'The new Record was successfully created!', 'success')
                Router.push('/admin/tables')
            } else {
                alert('OPERATION FAILED', 'New Record was not added, please try again.', 'error')
            }
        })
    }

  
  return (
    <>
      <Header />
      {/* Page content */}
      <Container className="mt--7" fluid>
        <Row>
          <Col className="mb-5 mb-xl-0" xl="8">
            <Card className="shadow">

              <CardHeader className="bg-transparent">
                <Row className="align-items-center">
                  <div className="col">
                    <h6 className="text-uppercase text-light ls-1 mb-1">
                      Add Record
                    </h6>
                   
                  </div>
                  <div className="col">
                   
                    
                  </div>
                </Row>
              </CardHeader>
              <CardBody>

               <Form onSubmit={(e) => createRecords(e)}>

        <FormGroup controlId="typeName" >
  
        <Input type="select" value={type} onChange={(e) => getCategories(e.target.value)} required >
                     <option value selected disabled>Select Category</option>
                    <option value="Income">Income</option>
                    <option value="expense">Expense</option>
        </Input>
      </FormGroup>

      <FormGroup controlId="categoryName">
      
        <Input type="select" value={categoryName} onChange={(e) => setCategoryName(e.target.value)} required>
           <option value selected disabled>Select Category Type</option>
                    {
                      categories.map((category) => {
                            return(
                                <option key={category.id} value={category.name}>
                                    {category.name}
                                </option>
                            )
                        })
                    }
        </Input>
      </FormGroup>


      <FormGroup controlId="amount">
        <Label>Amount</Label>
        <Input type="number" placeholder="Enter Amount" value={amount} onChange={(e) => setAmount(e.target.value)} required />
      </FormGroup>

      <FormGroup controlId="description">
        <Label >Description</Label>
        <Input type="text" placeholder="Enter Description" value={description} onChange={(e) => setDescription(e.target.value)} required />
      </FormGroup>

      <Button className="my-4" color="primary" type="submit">Submit</Button>

               </Form>
               
              </CardBody>
            </Card>
          </Col>
        </Row>

      </Container>
    </>
  );
};

Dashboard2.layout = Admin;

export default Dashboard2;
