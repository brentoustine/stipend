import React, { useState, useEffect } from "react";
// react component that copies the given text inside your clipboard
import { CopyToClipboard } from "react-copy-to-clipboard";

import Link from 'next/link'

import AppHelper from '../../app-helper'
// reactstrap components
import {
  Button,
  Card,
  CardHeader,
  CardBody,
  NavItem,
  NavLink,
  Nav,
  Progress,
  Table,
  Container,
  Row,
  Col,
  FormGroup,
  Form,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
  Input,
  Media,
  Badge
} from "reactstrap";
// layout for this page
import Admin from "layouts/Admin.js";
// core components
import Header from "components/Headers/Header.js";

const MyCategories = () => {

  const [typeName,setTypeName] = useState("") 
  const [data,setData] = useState([]) 

  useEffect(() => {
    const payload = {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${ AppHelper.getAccessToken()}`
          },
          body: JSON.stringify({value: typeName})
       }

       fetch(`https://morning-coast-98421.herokuapp.com/api/users/get-categories`, payload).then(AppHelper.toJSON).then(fetchedData => 
            setData(fetchedData)
            )
}, [])



  return (
    <>
     <Header />

     <Container className="mt--7" fluid>
        <Row>
          <Col className="mb-5 mb-xl-0" xl="8">
            <Card className="shadow">
              <CardHeader className="bg-transparent">
                <Row className="align-items-center">
                  <div className="col">
                    <h6 className="text-uppercase text-light ls-1 mb-1">
                      MY CATEGORIES
                    </h6>
                    <Link href="/admin/newcategory">
                    <Button className="my-4" color="primary" type="submit">
                  Add Category
                </Button>
                </Link>
                  </div>
                   <Table
                className="align-items-center table-dark table-flush"
                responsive
              >
                <thead className="thead-dark">
                  <tr>
                    <th scope="col">Category</th>
                    <th scope="col">Type</th>
                  
                    
                   
                    <th scope="col" />
                  </tr>
                </thead>
                <tbody>
                { data.map(dataMapped =>
                  <tr>
                    <th scope="row">
                      <Media className="align-items-center">
                       
                         <Media>
                          <span key={data.name} className="mb-0 text-sm">
                            {dataMapped.name}
                          </span>
                        </Media>
                      </Media>
                    </th>
                    <td key={data.type}>{dataMapped.type}</td>
                   
                 
                    
                  </tr>)}

        </tbody>
          </Table>

                </Row>
              </CardHeader>
              <CardBody>

                {/* Chart */}
                <div className="chart">
             
                
                </div>
              </CardBody>
            </Card>
          </Col>
        </Row>

      </Container>
     
    </>
  );
};

MyCategories.layout = Admin;

export default MyCategories;
