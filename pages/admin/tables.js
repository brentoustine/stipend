import React,{useState, useEffect} from "react";

import Link from 'next/link'

import AppHelper from '../../app-helper'

// reactstrap components
import {
  Button,
  Card,
  CardHeader,
  CardBody,
  NavItem,
  NavLink,
  Nav,
  Progress,
  Table,
  Container,
  Row,
  Col,
  FormGroup,
  Form,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
  Input,
  Media,
  Badge
} from "reactstrap";
// layout for this page
import Admin from "layouts/Admin.js";
// core components
import Header from "components/Headers/Header.js";

function Tables() {

const [typeName,setTypeName] = useState("") 
const [records,setRecords] = useState([]) 

useEffect(() => {
    const payload = {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${ AppHelper.getAccessToken()}`
          },
          body: JSON.stringify({value: typeName})
       }

       fetch(`https://morning-coast-98421.herokuapp.com/api/users/get-records`, payload).then(AppHelper.toJSON).then(fetchedData => {
        ;     console.log(fetchedData)
             setRecords(fetchedData.transactions.map(records => records))

            })
}, [])

  return (
    <>
      <Header />
      {/* Page content */}
      <Container className="mt--7" fluid>
        <Row>
          <Col className="mb-5 mb-xl-0" xl="8">
            <Card className="shadow">
              <CardHeader className="bg-transparent">
                <Row className="align-items-center">
                  <div className="col">
                   
                      <h4 className="text-uppercase text-dark ls-1 mb-3">
                      STIPEND RECORD
                       </h4>
            
                  </div>
                   <Table
                className="align-items-center table-dark table-flush"
                responsive
              >
                <thead className="thead-dark">
                  <tr>
                    <th scope="col">Category Name</th>
                    <th scope="col">Type</th>
                    <th scope="col">Description</th>
                    <th scope="col">Amount</th>
                    <th scope="col">Balance</th>
                  
                    
                   
                    <th scope="col" />
                  </tr>
                </thead>
                <tbody>
               
                  { records.map(record =>
                  <tr>
                    <th scope="row">
                      <Media className="align-items-center">
                       
                         <Media>
                          <span className="mb-0 text-sm">
                            {record.categoryName}
                          </span>
                        </Media>
                      </Media>
                    </th>
                    <td >{record.type}</td>
                     <td >{record.description}</td>
                      <td >{record.amount}</td>
                      <td >{record.balanceAfterTransaction}</td>
                   
                 
                    
                  </tr>)}

        </tbody>
          </Table>

                </Row>
              </CardHeader>
              <CardBody>

                {/* Chart */}
                <div className="chart">
             
                
                </div>
              </CardBody>
            </Card>
          </Col>
        </Row>

      </Container>
    </>
  );
}

Tables.layout = Admin;

export default Tables;
