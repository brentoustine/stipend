import React,{useState} from "react";
import Link from 'next/link';


import AppHelper from '../../app-helper';



import Admin from "layouts/Admin.js";
// core components
import Header from "components/Headers/Header.js";



import {
  Button,
  Container,
  Card,
  CardHeader,
  CardBody,
  FormGroup,
  Form,
  Input,
  InputGroupAddon,
  InputGroupText,
  InputGroup,
  Row,
  Col,
} from "reactstrap";


import Router from 'next/router';

// import Swal from 'sweetalert2';


function Maps() {

   const [categoryName, setCategoryName] = useState("")
    const [typeName, setTypeName] = useState(undefined)

    //create a function that will allow you to create a new category in the database. 
    const createCategory = (e) => {
        e.preventDefault() //to avoid page redirections

        //lets create an object that will describe the request options/payload of the request we want to send. 
        const payload = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${ AppHelper.getAccessToken() }`
            },
            body: JSON.stringify({
                name: categoryName,
                type: typeName
            }) 
        }

        //send the request to the backend project for processing
        fetch(`https://morning-coast-98421.herokuapp.com/api/users/add-category`, payload).then(AppHelper.toJSON).then(isSuccessful => {
            //i will describe the procedure upon recieving the response.
            if (isSuccessful === true) {
               Router.push('/admin/categories')
            } else {
              Swal.fire('Operation Failed','Something went wrong', 'error')
            }
        })
    }


  return (
    <>
      <Header />
    
    <Container className="mt--7" fluid>
        <Row>
          <Col className="mb-5 mb-xl-0" xl="8">
            <Card className="shadow">
             <CardHeader className="bg-transparent">
                <Row className="align-items-center">
                  <div className="col">
                    <h6 className="text-uppercase text-light ls-1 mb-1">
                      Add Category
                    </h6>
                   
                  </div>
                  <div className="col">
                   
                    
                  </div>
                </Row>
              </CardHeader>
          {/*<CardHeader className="bg-transparent pb-5">
            <div className="text-muted text-center mt-2 mb-3">
     
            </div>
           <p>ADD CATEGORY</p>
          </CardHeader>*/}
          <CardBody>
         

            <Form onSubmit={ (e) => createCategory(e)} >

              <FormGroup controlId="categoryName" className="mb-3">

                <InputGroup className="input-group-alternative">
                  <InputGroupAddon addonType="prepend">
                    <InputGroupText>
                      <h5>Category Name</h5>
                    </InputGroupText>
                  </InputGroupAddon>
                  <Input
                   value={categoryName} 
                   onChange={ (e) => setCategoryName(e.target.value)}
                    placeholder="Enter Category Name"
                    type="text"
                    required
                  />
                </InputGroup>
              </FormGroup>

              <FormGroup controlId="typeName">
                <InputGroup className="input-group-alternative">
                  <InputGroupAddon addonType="prepend">
                    <InputGroupText>
                      <h5>Category Type</h5>
                    </InputGroupText>
                  </InputGroupAddon>
                  <Input
                  type="select" 
                  value={typeName} 
                  onChange={ (e) => setTypeName(e.target.value)} 
                  required
                  >
                  <option value selected disabled>Select Category</option>
                    <option value="Income">Income</option>
                    <option value="Expense">Expense</option>
                    </Input>

                </InputGroup>

              </FormGroup>
             
              <div className="text-center">
                <Button className="my-4" color="primary" type="submit">
                  Submit Category
                </Button>
              </div>
            </Form>
          </CardBody>
        </Card>
      </Col>
      </Row>
      </Container>
      
       
    </>
  );
}

Maps.layout = Admin;

export default Maps;
