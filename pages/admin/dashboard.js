import {useState, useEffect} from 'react'
import { Pie } from 'react-chartjs-2'
import AppHelper from '../../app-helper'
import moment from 'moment'
// node.js library that concatenates classes (strings)
import classnames from "classnames";




// reactstrap components
import {
  Button,
  Card,
  CardHeader,
  CardBody,
  NavItem,
  NavLink,
  Nav,
  Progress,
  Table,
  Container,
  Row,
  Col,
  FormGroup,
  Form,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
  Input
} from "reactstrap";
// layout for this page
import Admin from "layouts/Admin.js";



import Header from "components/Headers/Header.js";

const Dashboard = () => {

  const [labelsArr, setLabelsArr] = useState([])
  const [dataArr, setDataArr] = useState([])
  const [fromDate, setFromDate] = useState(moment().subtract(1, 'months').format('YYYY-MM-DD'))
  const [toDate, setToDate] = useState(moment().format('YYYY-MM-DD'))


    const data = {
        labels: labelsArr,
        datasets: [
            {
                data: dataArr,
                backgroundColor: [
                    '#003f5c',
                    '#2f4b7c',
                    '#665191',
                    '#a05195',
                    '#d45087',
                    '#f95d6a',
                    '#ff7c43',
                    '#ffa600'
                ],
                hoverBackgroundColor: [
                    '#ffa600',
                    '#ff7c43',
                    '#f95d6a',
                    '#d45087',
                    '#a05195',
                    '#665191',
                    '#2f4b7c',
                    '#003f5c'
                ]
            }
        ]
    }



    useEffect(() => {
        const payload = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${ AppHelper.getAccessToken() }`
            }, 
            body: JSON.stringify({
                fromDate: fromDate,
                toDate: toDate
            })
        }

          fetch(`https://morning-coast-98421.herokuapp.com/api/users/get-records-breakdown-by-range`, payload).then(AppHelper.toJSON).then(records => {
            setLabelsArr(records.map(record => record.categoryName))
            setDataArr(records.map(record => record.totalAmount))
        })
    }, [fromDate, toDate])



  
  return (
    <>
      <Header />
      {/* Page content */}
      <Container className="mt--7" fluid>
        <Row>
          <Col className="mb-5 mb-xl-0" xl="8">
            <Card className="shadow">
              <CardHeader className="bg-transparent">
                <Row className="align-items-center">
                  <div className="col">
                    <h6 className="text-uppercase text-light ls-1 mb-1">
                      Overview
                    </h6>
                   
                  </div>
                  <div className="col">
                   
                    <FormGroup  className="mb-3">
                <InputGroup className="input-group-alternative">
                  <InputGroupAddon addonType="prepend">
                  </InputGroupAddon>
                  <Input
                    className="w-50 m-1"
                    onChange={ (e) => setFromDate(e.target.value) }
                    value={fromDate}
                    type="date"
                    
                  />
                </InputGroup>
                </FormGroup>
                  </div>

                   <div className="col">
                   
                    <FormGroup  className="mb-3">
                <InputGroup className="input-group-alternative">
                  <InputGroupAddon addonType="prepend">
                  </InputGroupAddon>
                  <Input
                    className="w-50 m-1"
                    onChange={ (e) => setToDate(e.target.value) }
                    value={toDate}
                    type="date"
                    
                  />
                </InputGroup>
                </FormGroup>
                  </div>

                </Row>
              </CardHeader>
              <CardBody>

                {/* Chart */}
                <div>
                <Pie data={data} height={100}/>
                
                </div>
              </CardBody>
            </Card>
          </Col>
        </Row>

      </Container>
    </>
  );
};

Dashboard.layout = Admin;

export default Dashboard;
