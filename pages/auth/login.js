import { useState, useContext } from 'react'
import Swal from 'sweetalert2'
import Router from 'next/router'

// reactstrap components
import {
  Button,
  Card,
  CardHeader,
  CardBody,
  FormGroup,
  Form,
  Input,
  InputGroupAddon,
  InputGroupText,
  InputGroup,
  Row,
  Col,
} from "reactstrap";
// layout for this page
import Auth from "layouts/Auth.js";

function Login() {

  const [email, setEmail] = useState("")
 const [password, setPassword] = useState("")
 

   

   //lets create a function to retrieve the user details before the client can be redirected inside the rECORDS page. 
  const retrieveUserDetails = (accessToken) => {
     //we have to make sure the user has already been verified meaning it was already granted access token.
     const options = {
         headers: { Authorization: `Bearer ${accessToken}`}
     }
     //create a request going to the desired enspoint with the payload.
     fetch('https://morning-coast-98421.herokuapp.com/api/users/details', options).then((response) => response.json()).then(data => {
       
         Router.push('/admin/dashboard') 
     })
  }

  function login(e){
    e.preventDefault()
    //describe the request to login
    fetch("https://morning-coast-98421.herokuapp.com/api/users/login", {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        email: email,
        password: password
      }) //for the request to be accepted by the api
    }).then(res => res.json()).then(data => {
      //lets create a control structure
      if(typeof data.accessToken !== "undefined") {
        //store the access token in the local storage
        localStorage.setItem('token', data.accessToken)
        retrieveUserDetails(data.accessToken)
        Swal.fire({
          icon: 'success',
          title: 'Successfully Logged In'
        })
    
      } else {
           Swal.fire('Login Error', 'You may have registered using a different login procedure', 'error')
      }
    })
  }

  return (
    <>

      <Col lg="5" md="7">
        <Card className="bg-secondary shadow border-0">
          <CardHeader className="bg-transparent pb-5">
            <div className="text-muted text-center mt-2 mb-3">
              {/*<small>Sign in with</small>*/}
            </div>
            <div className="btn-wrapper text-center">
          {/*    <Button
                className="btn-neutral btn-icon"
                color="default"
                href="#pablo"
                onClick={(e) => e.preventDefault()}
              >
                <span className="btn-inner--icon">
                  <img
                    alt="..."
                    src={require("assets/img/icons/common/github.svg")}
                  />
                </span>
                <span className="btn-inner--text">Github</span>
              </Button>*/}
             {/* <Button
                className="btn-neutral btn-icon"
                color="default"
                href="#pablo"
                onClick={(e) => e.preventDefault()}
              >
                <span className="btn-inner--icon">
                  <img
                    alt="..."
                    src={require("assets/img/icons/common/google.svg")}
                  />
                </span>
                <span className="btn-inner--text">Google</span>
              </Button>*/}
            </div>
          </CardHeader>
          <CardBody className="px-lg-5 py-lg-5">
            <div className="text-center text-muted mb-4">
              <small>Sign In with credentials</small>
            </div>

            <Form onSubmit={e => login(e)}>

              <FormGroup controlId="email" className="mb-3">

                <InputGroup className="input-group-alternative">
                  <InputGroupAddon addonType="prepend">
                    <InputGroupText>
                      <i className="ni ni-email-83" />
                    </InputGroupText>
                  </InputGroupAddon>
                  <Input
                    onChange={e => setEmail(e.target.value)}
                    value={email}
                    placeholder="Email"
                    type="email"
                    autoComplete="new-email"
                    required
                  />
                </InputGroup>
              </FormGroup>

              <FormGroup controlId="password">
                <InputGroup className="input-group-alternative">
                  <InputGroupAddon addonType="prepend">
                    <InputGroupText>
                      <i className="ni ni-lock-circle-open" />
                    </InputGroupText>
                  </InputGroupAddon>
                  <Input
                    value={password} 
                    onChange={e=>setPassword(e.target.value)} 
                    placeholder="Password"
                    type="password"
                    autoComplete="new-password"
                    required
                  />
                </InputGroup>
              </FormGroup>
              <div className="custom-control custom-control-alternative custom-checkbox">
                <input
                  className="custom-control-input"
                  id=" customCheckLogin"
                  type="checkbox"
                />
                <label
                  className="custom-control-label"
                  htmlFor=" customCheckLogin"
                >
                  <span className="text-muted">Remember me</span>
                </label>
              </div>
              <div className="text-center">
                <Button className="my-4" color="primary" type="submit">
                  Sign in
                </Button>
              </div>
            </Form>
          </CardBody>
        </Card>
        <Row className="mt-3">
          <Col xs="6">
            <a
              className="text-light"
              href="#pablo"
              onClick={(e) => e.preventDefault()}
            >
              {/*<small>Forgot password?</small>*/}
            </a>
          </Col>
          <Col className="text-right" xs="6">
            <a
              className="text-light"
              href="/auth/register"
            >
              <small>Create new account</small>
            </a>
          </Col>
        </Row>
      </Col>
    </>
  );
}

Login.layout = Auth;

export default Login;
