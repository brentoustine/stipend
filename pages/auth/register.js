import { useState, useEffect } from "react";

import Router from 'next/router'

import Swal from 'sweetalert2';

// reactstrap components
import {
  Button,
  Card,
  CardHeader,
  CardBody,
  FormGroup,
  Form,
  Input,
  InputGroupAddon,
  InputGroupText,
  InputGroup,
  Row,
  Col,
} from "reactstrap";
// layout for this page
import Auth from "layouts/Auth.js";

function Register() {


   const [firstName, setFirstName]= useState("")
   const [lastName, setLastName]= useState("")
   const [mobileNo, setMobileNo]= useState(0)  
   const [email, setEmail] = useState("") //assign an initial value.
   const [password1, setPassword1] = useState("")
   const [password2, setPassword2] = useState("")

   const [btnActive, setBtnActive] = useState(false)

   function registerUser(e){
     e.preventDefault() //what does this do? this prevents page redirection.
     
     //send a request to the our register end point
     fetch('https://morning-coast-98421.herokuapp.com/api/users/register', {
       method: 'POST',
       headers: {
          'Content-Type': 'application/json'
       },
       body: JSON.stringify({
          firstName: firstName,
          lastName: lastName,
          email: email,
          mobileNo: mobileNo,
          password: password1
       })
     }).then(res => res.json()).then(convertedData => {
        console.log(convertedData) //true or false
        if(convertedData === true){
           Swal.fire({
           icon: "success",
           title: "Successfully Registered!",
           text: "Thank you for registering."
           })
           //clear the input fields
           setEmail("")
           setPassword1("")
           setPassword2("")
           setFirstName("")
           setLastName("")
           setMobileNo(0)

           Router.push('/auth/login')
         } else {
           Swa.fire({
             icon: "error",
             title: "Registration Failed!",
             text: "Something went wrong, Please Try Again Later."
           })
         }
        //lets create a prompt message that the user will read upon sending the request
        
     })
  
  }

 //our next goal is to change the state of our register button so that we will be allowed to proceed to the next step.

 //in our effect hook lets include the new components to make sure that the user would fill in data first before being allowed to proceed with the next procedure. 
  useEffect(() => {
    //we want to validate if all input fields are filled. 
    //we want to validate if password will match
    if((firstName !== "" && lastName !== "" && mobileNo !== "" && email !== "" && password1!== "" && password2!== "") && (password2 === password1)) {
       setBtnActive(true)      
    } else {
       setBtnActive(false)
    }
  })


  return (
    <>
      <Col lg="6" md="8">
        <Card className="bg-secondary shadow border-0">
          <CardHeader className="bg-transparent pb-5">
            <div className="text-muted text-center mt-2 mb-4">
              <small>Register an Account</small>
            </div>
            <div className="text-center">

             {/* <Button
                className="btn-neutral btn-icon mr-4"
                color="default"
                href="#pablo"
                onClick={(e) => e.preventDefault()}
              >
                <span className="btn-inner--icon">
                  <img
                    alt="..."
                    src={require("assets/img/icons/common/github.svg")}
                  />
                </span>
                <span className="btn-inner--text">Github</span>
              </Button>*/}
            {/*  <Button
                className="btn-neutral btn-icon"
                color="default"
                href="#pablo"
                onClick={(e) => e.preventDefault()}
              >
                <span className="btn-inner--icon">
                  <img
                    alt="..."
                    src={require("assets/img/icons/common/google.svg")}
                  />
                </span>
                <span className="btn-inner--text">Google</span>
              </Button>*/}
            </div>
          </CardHeader>
          <CardBody className="px-lg-5 py-lg-5">
            <div className="text-center text-muted mb-4">
              <small>Sign up with credentials</small>
            </div>

            <Form onSubmit={(e) => registerUser(e)}>

               {/*FIRST NAME*/}
              <FormGroup>
                <InputGroup className="input-group-alternative mb-3">
                  <InputGroupAddon addonType="prepend">
                    <InputGroupText>
                      <i className="ni ni-hat-3" />
                    </InputGroupText>
                  </InputGroupAddon>
                  <Input
                  onChange={e => setFirstName(e.target.value)} 
                   value={firstName}
                  placeholder="First Name" 
                  type="text"
                  required 
                  />
                </InputGroup>
              </FormGroup>

              {/*LAST NAME*/}
              <FormGroup>
                <InputGroup className="input-group-alternative mb-3">
                  <InputGroupAddon addonType="prepend">
                    <InputGroupText>
                      <i className="ni ni-hat-3" />
                    </InputGroupText>
                  </InputGroupAddon>
                  <Input
                   onChange={e => setLastName(e.target.value)}
                   value={lastName}
                    placeholder="Last Name"
                    type="text"
                    autoComplete="new-lastname"
                    required
                  />
                </InputGroup>
              </FormGroup>

              {/*MOBILE NUMBER*/}
              <FormGroup>
                <InputGroup className="input-group-alternative mb-3">
                  <InputGroupAddon addonType="prepend">
                    <InputGroupText>
                      <i className="ni ni-mobile-button" />
                    </InputGroupText>
                  </InputGroupAddon>
                  <Input
                    onChange={e => setMobileNo(e.target.value)}
                    value={mobileNo}
                    placeholder="Mobile Number"
                    type="number"
                    autoComplete="Mobile #"
                    required
                  />
                </InputGroup>
              </FormGroup>

                {/*EMAIL*/}
              <FormGroup>
                <InputGroup className="input-group-alternative mb-3">
                  <InputGroupAddon addonType="prepend">
                    <InputGroupText>
                      <i className="ni ni-email-83" />
                    </InputGroupText>
                  </InputGroupAddon>
                  <Input
                    onChange={action => setEmail(action.target.value)} 
                    value={email}
                    placeholder="Email"
                    type="email"
                    autoComplete="new-email"
                    required
                  />
                </InputGroup>
              </FormGroup>


             {/*PASSWORD*/}
              <FormGroup>
                <InputGroup className="input-group-alternative">
                  <InputGroupAddon addonType="prepend">
                    <InputGroupText>
                      <i className="ni ni-lock-circle-open" />
                    </InputGroupText>
                  </InputGroupAddon>
                  <Input
                    onChange={gawa => setPassword1(gawa.target.value)} 
                    value={password1}
                    placeholder="Password"
                    type="password"
                    autoComplete="new-password"
                    required
                  />
                </InputGroup>
              </FormGroup>

              {/*CONFIRM PASSWORD*/}
              <FormGroup>
                <InputGroup className="input-group-alternative">
                  <InputGroupAddon addonType="prepend">
                    <InputGroupText>
                      <i className="ni ni-lock-circle-open" />
                    </InputGroupText>
                  </InputGroupAddon>
                  <Input
                    onChange={gawa => setPassword2(gawa.target.value)} 
                    value={password2}
                    placeholder="Confirm Password"
                    type="password"
                    autoComplete="new-password"
                    required
                  />
                </InputGroup>
              </FormGroup>

              <div className="text-muted font-italic">
                <small>
                  required:{" "}
                  <span className="text-success font-weight-700">Password must contain atleast 1 special character and 1 capital letter</span>
                </small>
              </div>
              <Row className="my-4">
                <Col xs="12">
                  <div className="custom-control custom-control-alternative custom-checkbox">
                    <input
                      className="custom-control-input"
                      id="customCheckRegister"
                      type="checkbox"
                      required
                    />
                    <label
                      className="custom-control-label"
                      htmlFor="customCheckRegister"
                    >
                      <span className="text-muted">
                        I agree with the{" "}
                        <a href="#pablo" onClick={(e) => e.preventDefault()}>
                          Privacy Policy
                        </a>
                      </span>
                    </label>
                  </div>
                </Col>
              </Row>
              <div className="text-center">
              {
                btnActive ?
                <Button className="mt-4" color="primary" type="submit">
                  Create account
                </Button>
                :
                <Button className="mt-4" color="primary" type="submit" disabled>
                  Create account
                </Button>
              }
                
              </div>
            </Form>
          </CardBody>
        </Card>
      </Col>
    </>
  );
}

Register.layout = Auth;

export default Register;
